/* START TASK 1: Your code goes here */

let table = document.querySelector('table');
let specCell = document.getElementById('special-cell');
        specCell.addEventListener('click', function() {
            table.classList.toggle('green');
        }, false);
        
let tr = document.querySelectorAll('tr');
tr.forEach(function(item) {
    item.addEventListener('click', function() {
        item.classList.toggle('blue');
    }, false);
});
        
let tds = document.querySelectorAll('td');
tds.forEach(function(item) {
    item.addEventListener('click', function() {
        this.classList.toggle('yellow');
    }, false);
});

/* END TASK 1 */

/* START TASK 2: Your code goes here */

// let re = /^\+380[0-9]{9}/;
// function phoneCheck(str) {
//     return re.test(str);
// }

// let phoneNumber = document.getElementById("phone");
// let not = document.getElementById("notification");
// let inp = document.getElementById("submit");
// phoneNumber.addEventListener("input", function() {
//     if (phoneCheck(phoneNumber.value)) {
//         inp.disabled = false;
//         not.textContent = "";
//         phoneNumber.classList.remove("red");
//         not.classList ='success';
//     } else {
//         not.classList ='error';
//         not.textContent = "Type number does not follow format +380*********";
//         phoneNumber.classList ='red';
//     }
// }, false)

// let btn = document.getElementById("submit")
// btn.addEventListener("click", function() {
//     not.classList ='success-sent';
//     not.textContent = "Data was successfully sent"
// }, false)

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
