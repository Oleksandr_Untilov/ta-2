function getAge(birthdayDate) {
    let now = new Date();
    let birthday =new Date(birthdayDate);
    let age = now.getFullYear() - birthday.getFullYear();
    let difMonth = now.getMonth() - birthday.getMonth();
    let difDate = now.getDate() - birthday.getDate();
    if (difMonth < 0 || difMonth === 0 && difDate < 0) {
        age--;
    }
    return age;
}

function getWeekDay(dateOrTimestamp) {
    let date = new Date(dateOrTimestamp);
    let weekDay = date.toLocaleString('en', { weekday: 'long' });
    return weekDay;
}

function getAmountDaysToNewYear() {
    let now = new Date();
    let newYear = new Date(now.getFullYear() + 1, 0, 1);
    console.log(newYear);
    let milsecToNewYear = newYear - now;
    let daysToNewYear = Math.round(milsecToNewYear / 86400000);
    return daysToNewYear;
}

function getProgrammersDay(yearDate) {
    let programmersDay = new Date(yearDate, 0, 256);
    let day = programmersDay.toLocaleString('en-US', { day: 'numeric'});
    let month = programmersDay.toLocaleString('en-US', { month: 'short'});
    let year = programmersDay.toLocaleString('en-US', {year: 'numeric'});
    return day + ' ' + month + ', ' + year + ' (' + getWeekDay(programmersDay) + ')';
}

function howFarIs(specifiedWeekDay) {
    
    let days = {
        Sunday: 0,
        Monday: 1,
        Tuesday: 2,
        Wednesday: 3,
        Thursday: 4,
        Friday: 5,
        Saturday: 6
    }

    specifiedWeekDay = specifiedWeekDay.toLowerCase();
    specifiedWeekDay = specifiedWeekDay[0].toUpperCase() + specifiedWeekDay.substring(1);
    let specifiedWeekDayNumber = days[specifiedWeekDay];
    let now = new Date();
    let nowWeekDayNumber = now.getDay();
    if (specifiedWeekDayNumber < nowWeekDayNumber) {
        specifiedWeekDayNumber = specifiedWeekDayNumber + 7;
    } 
    let number = specifiedWeekDayNumber - nowWeekDayNumber;
    if (number === 0) {
        return `Hey, today is ${specifiedWeekDay} =)`;
    } else {
        return `It's ${number} days left till ${specifiedWeekDay}`;
    }
}

function isValidIdentifier(id) {
    let re = /^[a-zA-Z$_][\w$_]*$/g;
    let OK = re.test(id);
    return OK;
}

function capitalize(str) {
    const inputString = str.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
    return inputString; 
}

function isValidAudioFile(str) {
    let regexNameAudioFile = /^[a-zA-Z]+(.mp3|.flac|.alac|.aac)/g;
    let OK = regexNameAudioFile.test(str);
    return OK;
}

function getHexadecimalColors(str) {
    let re = /(#[0-9a-fA-F]{3};)|(#[0-9a-fA-F]{6};)/g;
    let matches = str.match(re);
    return matches;
    }

function isValidPassword(password) {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])[a-zA-Z0-9]{8,}$/g;
    return re.test(password);
}

function addThousandsSeparators(number) {
    const re = /\B(?=(\d{3})+(?!\d))/g;
    const result = String(number).replace(re, ',')
    return result;
}

function getAllUrlsFromText(text) {
    let matches = text.match(/\bhttps?::\/\/\S+/gi) || text.match(/\bhttps?:\/\/\S+/gi);
    return matches;
}
