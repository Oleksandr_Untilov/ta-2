const appRoot = document.getElementById('app-root');

// function displayData() {
// 	let data=['Apple', 'Banana', 'Kiwi'];
// 	let output="";
// 	let output2="";
// 	let dataList;
	
// 	for(let i=0; i< data.length; i++) {
// 		dataList=data[i];
// 		output+= '<input type="checkbox" value='+dataList+' name="box2">' + 
//         '   ' + dataList+'   '+'<br><br>';
// 		output2+= 'yes:<input type="radio" value="yes" name="'+data[i]+'">'+
//         'no:<input type="radio" value="yes" name="'+data[i]+'">'+'<br><br>';
// 		document.getElementById("app-root").innerHTML=output;
// 		document.getElementById("radioBtn").innerHTML=output2;
// 	}
// }
let form = document.createElement('form');
form.display = 'table';
form.setAttribute('margin', '0 auto');
form.setAttribute('width', '400px');
form.setAttribute('align', 'center');
// form.innerText = 'Countries Search';
let h1 = document.createElement('h1');
h1.innerText = 'Countries Search';
let field_1 = document.createElement('fieldset')
field_1.display = 'table-row';
// field_1.setAttribute('disabled', 'true');
let p_1 = document.createElement('p');
p_1.setAttribute('align', 'left');
p_1.setAttribute('width', '200px');
// p_1.innerText = 'Please choose type of search: ';
let label = document.createElement('label');
label.htmlFor = 'type-search';
label.display = 'table-cell';
let description = document.createTextNode('Please choose type of search: ');
description.display = 'table-cell';
label.appendChild(description);
let div_1 = document.createElement('div');
div_1.display = 'table-cell';
div_1.id = 'type-search';
div_1.setAttribute('align', 'left');
div_1.setAttribute('width', '200px');


let radioButton_1 = document.createElement('input');
radioButton_1.type = 'radio';
radioButton_1.id = 'by-region';
radioButton_1.name = 'choice';
radioButton_1.value = 'region';

let label_1 = document.createElement('label');
label_1.htmlFor = 'by-region';
let description_1 = document.createTextNode('By Region');
label_1.appendChild(description_1);

let radioButton_2 = document.createElement('input');
radioButton_2.type = 'radio';
radioButton_2.id = 'by-language';
radioButton_2.name = 'choice';
radioButton_2.value = 'language';

let newLine = document.createElement('br');

let label_2 = document.createElement('label');
label_2.htmlFor = 'by-language'
let description_2 = document.createTextNode('By Language');
label_2.htmlFor = 'by-language'
label_2.appendChild(description_2);

let field_2 = document.createElement('fieldset')
field_2.display = 'inline-block';
field_2.setAttribute('disabled', 'true');
let p_2 = document.createElement('p');
p_2.setAttribute('align', 'left');
p_2.setAttribute('width', '200px');
// p_2.setAttribute('disabled', 'true');
p_2.innerText = 'Please choose search query: ';
p_2.setAttribute('disabled', 'true');
let div_2 = document.createElement('div');
div_2.display = 'inline-block';
// div_2.setAttribute('align', 'left');
div_2.setAttribute('width', '200px');


let dropdown_1 = document.createElement('select'); 
dropdown_1.id = 'search-query';
dropdown_1.name = 'select-query';
dropdown_1.value = 'Select value';
// label_2.appendChild(description_2);

form.appendChild(h1);
form.appendChild(field_1);
field_1.appendChild(p_1);
p_1.appendChild(label);
p_1.appendChild(div_1);
div_1.appendChild(radioButton_1);
div_1.appendChild(label_1);
div_1.appendChild(newLine);
div_1.appendChild(radioButton_2);
div_1.appendChild(label_2);


form.appendChild(field_2);
field_2.appendChild(p_2);
field_2.appendChild(div_2);
div_2.appendChild(dropdown_1);
// div_2.appendChild(label_2);
// form.appendChild(field_2);
appRoot.appendChild(form);

function generate_table() {
  // get the reference for the body
//   let body = document.getElementsByTagName("body")[0];

  // creates a <table> element and a <tbody> element
  let tbl = document.createElement("table");
  let tblBody = document.createElement("tbody");

  // creating all cells
  for (let i = 0; i < 6; i++) {
    // creates a table row
    let row = document.createElement("tr");

    for (let j = 0; j < 2; j++) {
      // Create a <td> element and a text node, make the text
      // node the contents of the <td>, and put the <td> at
      // the end of the table row
      let cell = document.createElement("td");
      let cellText = document.createTextNode(externalService.getCountryListByRegion());
      cell.appendChild(cellText);
      row.appendChild(cell);
    }

    // add the row to the end of the table body
    tblBody.appendChild(row);
  }

  // put the <tbody> in the <table>
  tbl.appendChild(tblBody);
  // appends <table> into <body>
  appRoot.appendChild(tbl);
  // sets the border attribute of tbl to 2;
//   tbl.setAttribute("border", "1");
}

generate_table();

// appRoot.appendChild(form);


// 'Please choose type of search'

// const field_2 = document.getElementById('App-field-2');
// form.append(field_1);
// form.append(field_2);
// appRoot.append(form);



/*
write your code here

list of all regions
*/
externalService.getRegionsList();
console.log(externalService.getRegionsList());
// list of all languages
externalService.getLanguagesList();
// get countries list by language
externalService.getCountryListByLanguage()

// const countries = externalService.getCountryListByLanguage("English");
// console.log(countries);
// get countries list by region
externalService.getCountryListByRegion()
