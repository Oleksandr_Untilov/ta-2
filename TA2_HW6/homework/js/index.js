function visitLink(path) {
    let numberClicks = localStorage.getItem(path);
    localStorage.setItem(path, Number(numberClicks)+1)
}

function viewResults() {
    const parentDOM = document.getElementById('content');
	const ul = document.createElement('ul');
    ul.innerHTML = '';
    for(let i =0;i<3;i++){
		const li = document.createElement('li');
        li.innerHTML = 'You visited Page' + (i+1) + ' '
		+ Number(localStorage.getItem('Page' + (i+1))) + ' time(s)';
		ul.append(li);
    }
	parentDOM.append(ul);
	localStorage.clear();
}
