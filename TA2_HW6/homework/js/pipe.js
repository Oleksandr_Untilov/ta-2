function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...funcs) => {
	return funcs.reduce((result, func) => {
		return isFunction(func) ? func(result) : 'Provided argument at position 2 is not a function!'
	}, value);
};

