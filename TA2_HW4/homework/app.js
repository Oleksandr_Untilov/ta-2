function reverseNumber(num) {
    let result = 0;
    if (num < 0) {
        num *= -1;
        flagNegative = true;
    }
    while (num > 0) {
        digit = +num % 10;
        result = +result * 10 + digit;
        num = num / 10 | 0;
    }
    if (flagNegative) {
        return -result
    } else {
        return result
    }
}
    
function forEach(arr, func) {
    for (let i=0; i<arr.length; i++) {
        func(arr[i])
    }
}

function map(arr, func) {
    let mappedArr = [];
    forEach(arr, function (el) {
        mappedArr.push(func(el))
    })
    return mappedArr
}

function filter(arr, func) {
    let filteredArr = [];
    forEach(arr, function(el) {
        if (func(el)) {
            filteredArr.push(el)
        }
    })
    return filteredArr
}

function getAdultAppleLovers(data) {
    let nameAdultAppleLovers = []
        data.filter(el => el.age > +18 && el.favoriteFruit === "apple")
        .map(el => nameAdultAppleLovers.push(el.name))
    return nameAdultAppleLovers;
}

function getKeys(obj) {
    let arrKeys =[];
    for (let key in obj) {
        arrKeys.push(key)
    }
    return arrKeys;
}

function getValues(obj) {
    let arrValues =[];
    for (let key in obj) {
        arrValues.push(obj[key])
    }
    return arrValues;
}

function showFormattedDate(dateObj) {
    let date = dateObj.getDate();
    let month = dateObj.toLocaleString('en', { month: 'short' });
    let year = dateObj.getFullYear();
    return "It is " + date + " of " + month + ", " + year
}
