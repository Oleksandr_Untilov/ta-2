let playingGame;
let totalPrize = 0; // total gain
let k = 1; // game number
let isContinue = true;


function game() {

    while (isContinue) {
        let maxNumber = 8 + 4 * (k - 1)
        let randomNumber = Math.floor(Math.random() * maxNumber);
        for (let j=0; j<3; j++) {
            let attemptsLeft = 3 - j;
            let possiblePrize = +(100 * Math.pow(2, k - 1) / Math.pow(2,j));
            let inputNumber = window.prompt('Choose a roulette pocket number from 0 to ' + maxNumber + '\n' + 
            'Attemps left: ' + attemptsLeft + '\n' +
            'Total prize: ' + totalPrize + '\n' +
            'Possible prize on current attempt: ' + possiblePrize);
            if (+inputNumber === randomNumber) {
                totalPrize += possiblePrize;
                isContinue = window.confirm('Congratulation, you won! Your prize is: ' + 
                +totalPrize + '$. Do you want to continue?');
                if (isContinue) {
                    k += 1
                } else {
                    return totalPrize;
                }

            } else {
                  if (inputNumber === null) {
                      return totalPrize;
                  }
            }
        }
        k += 1;
        isContinue = window.confirm('Thank you for your participation. Your prize is: ' + 
        +totalPrize + '$. Do you want to continue?');
    }
}

playingGame = window.confirm('Do you want to play a game?')
if (playingGame) {
    game();
} else {
    window.alert('You did not become a billionaire, but can.');
}
