let isValid = true;
let totalProfit = 0;
let initialAmount;
let numberYears;
let percentageYear;
let yearProfit;
let totalAmount;

initialAmount = Number(window.prompt("Initial amount (only number can't  be less than 1000)?"));
if (initialAmount < 1000 || isNaN(initialAmount)) {
    isValid = false;
}
numberYears = Number(window.prompt("Number of years?"));
if (numberYears < 1 || !Number.isInteger(numberYears)) {
    isValid = false;
}
percentageYear = Number(window.prompt("Percentage of year?"));
if (percentageYear > 100 || isNaN(percentageYear)) {
    isValid = false;
}

totalAmount = initialAmount;

if (isValid) { 
    for (let i = 1; i <= numberYears; i++) {
        yearProfit = +(totalAmount * (percentageYear / 100)).toFixed(2);
        totalProfit += +yearProfit.toFixed(2);
        totalAmount += +yearProfit.toFixed(2);
    }
    window.alert("Initial amount: " + initialAmount + "\n" + "Number of years: "
    + numberYears + "\n" + "Percentage of year: " + percentageYear + "\n" 
    + "\n" + "Total Profit: " + totalProfit.toFixed(2) + "\n" + "Total Amount: " + totalAmount.toFixed(2));
} else {
    window.alert("Invalid Input Data");
}