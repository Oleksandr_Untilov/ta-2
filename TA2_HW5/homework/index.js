function isEquals(arg1, arg2) {
    return arg1 === arg2
}

function isBigger(arg1, arg2) {
    return arg1 > arg2
}

function storeNames() {
    return Array.prototype.slice.call(arguments, 0);
}

function getDifference(arg1, arg2) {
    if (isBigger(arg1, arg2)) {
        return arg1 - arg2
    } else {
        return arg2 - arg1
    }
}

function negativeCount(arr) {
    let countNegative = 0;
    function isNegative(num) {
        return num < 0;
    }
    for(let element of arr) {
        if (isNegative(element)) {
            countNegative += 1
        }
    }
    return countNegative;
}

function letterCount(string, character) {
    let countchar = 0;
    for (let i=0; i<string.length; i++ ) {
        if (string[i] === character) {
            countchar += 1
        }
    }
        return countchar;
}

function countPoints(scores) {
    let i = 0;
    let points = 0;
    for (i; i <scores.length; i++) {
        let each = scores[i].split(":");
        if (+each[0] > +each[1]) {
            points += 3;
        } else if (+each[0] === +each[1]) {
            points += 1;
        } else {
            points += 0;
        }
    }
    return points;
}
